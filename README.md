# Mini Project 7 Vector Database

## Overview
This project demonstrates data processing capabilities using a vector database. It includes data ingestion, query functionalities, and visualizations to provide insights into the ingested data.

## Features
- **Data Ingestion:** Ingest data efficiently into a vector database.
- **Query Functionality:** Perform complex queries and aggregations.
- **Visualizations:** Generate meaningful visualizations to represent the data insights.
- **Documentation:** Detailed documentation of the project's functionality and usage.


## Preparation
1. Create a new rust project use:
   ```
   cargo new <your_project_name>
   ```
2. cd to your <your_project_name> directory
3. Download the latest Qdrant image from Dockerhub using the following code:
   ```
   docker pull qdrant/qdrant
   ```
4. Run the docker
   ```
   docker run -p 6333:6333 -p 6334:6334 \
    -v $(pwd)/qdrant_storage:/qdrant/storage:z \
    qdrant/qdrant
   ```
5. In your terminal, add the dependencies using:
   ```
   cargo add qdrant-client anyhow tonic tokio serde-json --features tokio/rt-multi-thread
   ```
6. Navigate to your `src` folder, and modify your main.rs in your own way (include ingest, query, aggregation, and visualization)
7. Once you're happy with your `main.rs`, add a test function
8. Add a Makefile and .gitlab-ci.yml
9. Create a blank project in `Gitlab` without a README.md, following the instruction in pushing an existing folder to push your folder to `Gitlab`

## Results Preview
**Query**
![Alt text](imgs/image.png)
**Aggregation**
![Alt text](imgs/image1.png)
**Visualization**
![Alt text](genre_count.png)


## Reference
1. https://qdrant.tech/documentation/quick-start/#create-a-collection
2. https://github.com/qdrant/rust-client/tree/master