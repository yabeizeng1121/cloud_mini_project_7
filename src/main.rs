use anyhow::Result;
use plotters::prelude::*;
use plotters::style::{BLACK, BLUE, WHITE};
use qdrant_client::prelude::*;
use qdrant_client::qdrant::vectors_config::Config;
use qdrant_client::qdrant::{
    CreateCollection, Distance, PointStruct, SearchPoints, VectorParams, VectorsConfig,
};
use serde::{Deserialize, Serialize};
use serde_json::json;
use std::error::Error;

#[derive(Serialize, Deserialize, Debug)]
struct Movie {
    id: i32,
    title: String,
    genre: String,
    year: i32,
}

#[tokio::main]
async fn main() -> Result<()> {
    let config = QdrantClientConfig::from_url("http://localhost:6334");
    let client = QdrantClient::new(Some(config))?;

    let collection_name = "movies";
    client.delete_collection(collection_name).await.ok(); // Ignore error if collection doesn't exist

    client
        .create_collection(&CreateCollection {
            collection_name: collection_name.into(),
            vectors_config: Some(VectorsConfig {
                config: Some(Config::Params(VectorParams {
                    size: 2,
                    distance: Distance::Cosine.into(),
                    ..Default::default()
                })),
            }),
            ..Default::default()
        })
        .await?;

    let movies_data = vec![
        ("The Shawshank Redemption", "Drama", 1994),
        ("The Godfather", "Crime, Drama", 1972),
        ("The Dark Knight", "Action, Crime, Drama", 2008),
        ("The Godfather Part II", "Crime, Drama", 1974),
        ("12 Angry Men", "Crime, Drama", 1957),
        ("Schindler's List", "Biography, Drama, History", 1993),
        (
            "The Lord of the Rings: The Return of the King",
            "Adventure, Fantasy",
            2003,
        ),
        ("Pulp Fiction", "Crime, Drama", 1994),
        ("The Good, the Bad and the Ugly", "Western", 1966),
        (
            "The Lord of the Rings: The Fellowship of the Ring",
            "Adventure, Fantasy",
            2001,
        ),
        ("Fight Club", "Drama", 1999),
        ("Forrest Gump", "Drama, Romance", 1994),
        ("Inception", "Action, Adventure, Sci-Fi", 2010),
        (
            "The Lord of the Rings: The Two Towers",
            "Adventure, Fantasy",
            2002,
        ),
        (
            "Star Wars: Episode V - The Empire Strikes Back",
            "Action, Adventure, Fantasy",
            1980,
        ),
        ("The Matrix", "Action, Sci-Fi", 1999),
        ("Goodfellas", "Biography, Crime, Drama", 1990),
        ("One Flew Over the Cuckoo's Nest", "Drama", 1975),
        ("Seven Samurai", "Action, Adventure, Drama", 1954),
        ("Se7en", "Crime, Drama, Mystery", 1995),
        ("Life Is Beautiful", "Comedy, Drama, Romance", 1997),
        ("City of God", "Crime, Drama", 2002),
        ("The Silence of the Lambs", "Crime, Drama, Thriller", 1991),
        ("It's a Wonderful Life", "Drama, Family, Fantasy", 1946),
        (
            "Star Wars: Episode IV - A New Hope",
            "Action, Adventure, Fantasy",
            1977,
        ),
        ("Saving Private Ryan", "Drama, War", 1998),
        ("Spirited Away", "Animation, Adventure, Family", 2001),
        ("The Green Mile", "Crime, Drama, Fantasy", 1999),
        ("Interstellar", "Adventure, Drama, Sci-Fi", 2014),
        ("Parasite", "Comedy, Drama, Thriller", 2019),
    ];

    let mut movies = Vec::new();
    #[derive(Debug)]
    struct PayloadConversionError;

    impl std::fmt::Display for PayloadConversionError {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "Payload conversion error")
        }
    }

    impl Error for PayloadConversionError {}

    for (i, (title, genre, year)) in movies_data.iter().enumerate() {
        let movie = Movie {
            id: i as i32 + 1,
            title: title.to_string(),
            genre: genre.to_string(),
            year: *year,
        };
        let point = PointStruct::new(
            i as u64,
            vec![movie.year as f32, i as f32], // Adjust the vector if needed
            json!(movie)
                .try_into()
                .map_err(|_| PayloadConversionError)?,
        );
        movies.push(point);
    }
    client
        .upsert_points_blocking(collection_name, None, movies, None)
        .await?;

    // query

    let search_result = client
        .search_points(&SearchPoints {
            collection_name: collection_name.into(),
            vector: vec![2000.0, 15.0],
            limit: 10,
            with_payload: Some(true.into()),
            ..Default::default()
        })
        .await?;
    println!("Query result: {:?}", search_result);

    // aggregate
    let mut genre_count = std::collections::HashMap::new();
    for movie in search_result.result {
        if let Some(genres) = movie.payload.get("genre").and_then(|v| v.as_str()) {
            for genre in genres.split(", ") {
                *genre_count.entry(genre.to_string()).or_insert(0) += 1;
            }
        }
    }
    println!("Genre count: {:?}", genre_count);

    // plot

    let root = BitMapBackend::new("genre_count.png", (640, 480)).into_drawing_area();
    root.fill(&WHITE)?;

    let max_count = *genre_count.values().max().unwrap_or(&0);
    let genres: Vec<String> = genre_count.keys().cloned().collect();

    let mut chart = ChartBuilder::on(&root)
        .caption("Movie Genre Count", ("sans-serif", 40).into_font())
        .margin(5)
        .x_label_area_size(35)
        .y_label_area_size(40)
        .build_cartesian_2d(0..genres.len(), 0..max_count)?;

    chart
        .configure_mesh()
        .x_labels(genres.len())
        .x_label_formatter(&|x| genres.get(*x).unwrap_or(&"".to_string()).clone())
        .y_desc("Count")
        .draw()?;

    chart
        .draw_series(genre_count.iter().map(|(genre, count)| {
            let x = genres.iter().position(|g| g == genre).unwrap();
            Rectangle::new([(x, 0), (x + 1, *count)], BLUE.mix(0.5).filled())
        }))?
        .label("Count")
        .legend(move |(x, y)| Rectangle::new([(x, y - 5), (x + 10, y + 5)], BLUE.filled()));

    chart
        .configure_series_labels()
        .background_style(WHITE.mix(0.8))
        .border_style(BLACK)
        .draw()?;

    root.present().expect("Unable to write result to bitmap");
    Ok(())
}

mod tests {
    // Removed the unnecessary `use super::*;` line.

    #[test]
    fn test_movie_creation() {
        let test_movie = super::Movie {
            // Use `super` here to access `Movie` from the outer module
            id: 1,
            title: "Test Movie".to_string(),
            genre: "Test Genre".to_string(),
            year: 2021,
        };

        assert_eq!(test_movie.id, 1);
        assert_eq!(test_movie.title, "Test Movie");
        assert_eq!(test_movie.genre, "Test Genre");
        assert_eq!(test_movie.year, 2021);
    }
}
