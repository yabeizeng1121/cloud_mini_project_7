.PHONY: rust-version format lint test build run clean all

setup-dependencies:
	@echo "Checking and installing dependencies..."
	apt install pkg-config
	apt-get install libudev-dev

rust-version:
	@echo "Rust command-line utility versions:"
	rustc --version
	cargo --version
	rustfmt --version
	rustup --version
	cargo clippy --version

format:
	cargo fmt

lint:
	cargo clippy

test:
	cargo test

build:
	cargo build --release

run: build
	cargo run --release

clean:
	cargo clean

all: format lint test build
